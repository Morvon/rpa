import * as StoreSecret from "./store_secret.js";

define(["react", "splunkjs/splunk"], function(react, splunk_js_sdk){
  const e = react.createElement;

  class EditPage extends react.Component {
    constructor(props) {
      super(props);
      this.ready=false;
      this.tableHeader=this.buildRowTable("ID","Realm","Username","Password","Options");
      this.state = {
        tableBody: [this.tableHeader],
        showComponent: false,
      };
      console.log(this.state.showComponent)
      this.build();
    }

    async build(){
      var credentials = await StoreSecret.getCredentials(splunk_js_sdk);      
      var tableBodyy=[this.tableHeader];
      var ID=1;
      for(const credential of credentials){
        let data = credential.properties();
        tableBodyy.push(this.buildRowTable(ID,data['realm'],data['username'],data['encr_password']));
        ID++;
      }
      this.ready=true;
      this.setState(() =>({
        tableBody:tableBodyy
      }))
      console.log(this.state.tableBody)
      console.log(this.state.showComponent)
    }
    change(){
      console.log("im in");
      console.log(this.state.showComponent);
        this.setState(() =>({
          showComponent:true
        }));
      
    }
    buildRowTable(id,realm,username,password,options=null){
      if(options===null){
        options=e(
          "button",
          {
          onClick: this.change.bind(this)
          },
          "Change password");
      }
      return e("tr",null,[
        e("td",{class:'table5'}, id),
        e("td",{class:'table10'}, realm),
        e("td",{class:'table20'}, username),
        e("td",{class:'table50'}, password),
        e("td",{class:'table15'}, options)
      ]);
    }
    render() {
      let properties=[e("table", {class:'table'},this.state.tableBody)];
      if(this.state.showComponent){
        console.log("IM AN IN");
        properties.push(e(ChangePassword,null));
      }
      console.log("PROP",properties);
      return e("div",null,properties);
    }

  }
  class ChangePassword extends react.Component {
    constructor(props) {
      super(props);

    }
    render() {
        return e("div", {class:'modal'},'modal button text');
    }
  }
  return e(EditPage);
});

/*
import * as ChgPass from "./changepasswords.js";
    render() {
      let hhml=e("table", {class:'table'}, [
        this.tableHeader
      ]);
      console.log(hhml);
      if(this.ready){
        hhml=e("table", {class:'table'},this.state.tableBody);
      }
      console.log(hhml);
      if(this.state.showComponent){
        hhml+=e(ChgPass.ChangePassword)
      }
      console.log(hhml);
      return hhml;
      
    }


*/