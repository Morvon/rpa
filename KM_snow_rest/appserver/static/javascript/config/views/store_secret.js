"use strict";
import { promisify } from './util.js'
import * as Config from './configuration.js'
var app_name = "KM_snow_rest";

var application_name_space = {
    owner: "nobody",
    app: app_name,
    sharing: "app",
};
export async function perform(splunk_js_sdk, setup_options) {

    try {
        const service = Config.get_splunk_js_sdk_service(
                splunk_js_sdk,
                application_name_space,
        );
        var spas = service.storagePasswords();
        await promisify(spas.fetch)();
        console.log(spas);

        //console.log(spas);
        //let { usernameSnow,passwordSnow,usernameProxy,passwordProxy, ...properties } = setup_options;

        //var storagePasswords = service.storagePasswords();
        //var storagePass = service.storagePasswords(application_name_space);
        //console.log(storagePass);
        //console.log(storagePasswords);
        /*


        storagePasswords.create({
            name: usernameSnow, 
            realm: "KM_snow_rest_snow", 
            password: passwordSnow}, 
            function(err, storagePassword) {
              if (err) 
                  {console.warn(err);}
              else {
               console.log(storagePassword.properties());
               }
            }
        );
        storagePasswords.create({
            name: usernameProxy, 
            realm: "KM_snow_rest_proxy", 
            password: passwordProxy}, 
            function(err, storagePassword) {
              if (err) 
                  {console.warn(err);}
              else {
               console.log(storagePassword.properties());
               }
            }
        );
        await Config.complete_setup(service);

        await Config.reload_splunk_app(service, app_name);

        Config.redirect_to_splunk_app_homepage(app_name);
        */
        await Config.reload_splunk_app(service, app_name);
        } catch (error) {

        console.log('Error:', error);
        alert('Error:' + error);
    }
}

export async function getCredentials(splunk_js_sdk){
    try{
        const service = Config.create_splunk_js_sdk_service(
            splunk_js_sdk,
            application_name_space,
    );
    var credentials = await Config.get_credentials_app(service)
    console.log(credentials);
    return credentials;
    }catch(error){
        console.log('Error:', error);
        alert('Error:' + error);
    }
}