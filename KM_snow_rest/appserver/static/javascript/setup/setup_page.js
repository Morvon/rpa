"use strict";

var app_name = "./KM_snow_rest";
var folder_name = "setup";
require.config({
    paths: {
        myApp: "../../app/" + app_name + "/javascript/" + folder_name + "/views/app",
        react: "../../app/" + app_name + "/javascript/" + folder_name + "/vendor/react.production.min",
        ReactDOM: "../../app/" + app_name + "/javascript/" + folder_name + "/vendor/react-dom.production.min",
    },
    scriptType: "module",
});

require([
    "react", 
    "ReactDOM",
    "myApp",
    ], function(react, ReactDOM, myApp) {
    ReactDOM.render(myApp, document.getElementById('main_container'));
});
