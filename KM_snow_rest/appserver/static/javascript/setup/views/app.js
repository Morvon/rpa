import * as Setup from "./store_secret.js";

define(["react", "splunkjs/splunk"], function(react, splunk_js_sdk){
  const e = react.createElement;

  class SetupPage extends react.Component {
    constructor(props) {
      super(props);

      this.state = {
        usernameSnow: '',
        passwordSnow: '',
        usernameProxy: '',
        passwordProxy: ''
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
      this.setState({ ...this.state, [event.target.name]: event.target.value})
    }

    async handleSubmit(event) {
      event.preventDefault();

      await Setup.perform(splunk_js_sdk, this.state)
    }

    render() {
      return e("div", null, [
        e("h2", null, "Enter a information to complete app setup."),
        e("div", null, [
          e("form", { onSubmit: this.handleSubmit }, [
            e("h2", null, "Service Now"),
            e("label", null,[
                "Username ", e("input", {type: "text", name: "usernameSnow", value: this.state.usernameSnow, onChange: this.handleChange})
            ]),
            e("label", null, [
              "Password ",
              e("input", { type: "password", name: "passwordSnow", value: this.state.passwordSnow, onChange: this.handleChange })
            ]),
            e("h2", null, "Proxy"),
            e("label", null,[
                "Username ", e("input", {type: "text", name: "usernameProxy", value: this.state.usernameProxy, onChange: this.handleChange})
            ]),
            e("label", null, [
              "Password ",
              e("input", { type: "password", name: "passwordProxy", value: this.state.passwordProxy, onChange: this.handleChange })
            ]),
            e("input", { type: "submit", value: "Submit" })
          ])
        ])
      ]);
    }
  }

  return e(SetupPage);
});
