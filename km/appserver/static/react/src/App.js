const React = require('react');
const ReactDOM = require('react-dom');
const App = require('./main/App').default;

module.exports = {

  start: function (props) {
    ReactDOM.render(
      <App props={props} />,
      document.getElementById('root')
    );
  }
};


/*
import React from 'react';

class App extends React.Component {
    render() {
        return (
            <div>
                <p>Hello world!</p>
            </div>
        )
    }
}

export default App;

*/