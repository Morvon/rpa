import React from "react";
import 'bootstrap/dist/css/bootstrap.css'
import MyNavbar from "./elements/MyNavbar.js";
import Accounts from "./components/accounts.js";
import Proxy from "./components/proxy.js";
import Snow from "./components/snow.js";

class App extends React.Component {
  constructor({ props }) {
    super();
    this.state = {
      text: props.text,
      searchManager: props.SearchManager,
      mvc: props.mvc,
      splunk_js_sdk: props.splunk_js_sdk,
      searchResult: [],
      pagebuild: <Accounts props={{splunk_js_sdk:props.splunk_js_sdk}}/>
    };
    this.changePage = this.changePage.bind(this);
  }

  changePage = (id) => {
      if(id==1){
        this.setState({
          pagebuild: <Accounts props={{splunk_js_sdk:this.state.splunk_js_sdk}}/>
        });
      }else if(id==2){
        this.setState({
          pagebuild: <Proxy props={{splunk_js_sdk:this.state.splunk_js_sdk}} />
        });
      
      }else if(id==3){
      this.setState({
        pagebuild: <Snow props={{splunk_js_sdk:this.state.splunk_js_sdk}} />
      });
      }
  }

  render() {
    return (
        <>
        <MyNavbar
        changePage={this.changePage}
        >
        </MyNavbar>
        <br />
        <br />
        {this.state.pagebuild}
      </>
    );
  }
}

export default App;