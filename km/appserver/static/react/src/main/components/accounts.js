import React, { Component } from "react";
import * as StoreSecret from "../splunk/store_secret";
import {Table} from "react-bootstrap";
import MyModal from "../elements/MyModal";


class Accounts extends Component {
    constructor({ props }) {
        super();
        this.state = {
          splunk_js_sdk: props.splunk_js_sdk,
          accounts: [],
          pageOneready: false,
          showModalPageOne: false
        };
        this.realm="";
        this.handleClose = this.handleClose.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleSavePassword = this.handleSavePassword.bind(this);
        this.handleSaveUsername = this.handleSaveUsername.bind(this);
        this.save=this.handleSavePassword;
      }

      componentDidMount() {
        this.build();
      }

      handleClose = () =>  this.setState({ showModalPageOne: false })
      handleShow = (realm,pass) =>  {
        if(pass==true){
          this.modalProperties={
            title:"Change Password",
            msg:"Enter new password",
            type:"password"
          }
          this.save=this.handleSavePassword;
        }else{
          this.modalProperties={
            title:"Change Username",
            msg:"Enter new username",
            type:"text"
          }
          this.save=this.handleSaveUsername;
        }
        this.realm=realm;
        this.setState({ showModalPageOne: true })
    
      }
      async handleSavePassword(val) {
        await StoreSecret.setPasswordIntoCredentials(this.state.splunk_js_sdk,this.realm,val)
        .then(
          () => {
            this.setState({ showModalPageOne: false });
            this.build();
          }
        );

      }
      async handleSaveUsername(val) {
        await StoreSecret.setUsernameIntoCredentials(this.state.splunk_js_sdk,this.realm,val)
        .then(
          () => {
            this.setState({ showModalPageOne: false });
            this.build();
          }
        );

      }
      async build(){
        const credentials = await StoreSecret.getCredentials(this.state.splunk_js_sdk);
        let acc = [];
        let ID=1;
        for(const credential of credentials){
          let data = credential.properties();
          acc.push({ 
            id: ID,
            realm: data['realm'],
            username: data['username'],
            password: data['encr_password']
           });
          ID++;
        }
        this.setState({accounts:acc, pageOneready: true});
    }
    renderTableHeader() {
        let header = Object.keys(this.state.accounts[0])
        return header.map((key, index) => {
           return <th key={index}>{key.toUpperCase()}</th>
        })
     };
    renderTableData() {
        return this.state.accounts.map((account, index) => {
           const { id, realm, username, password } = account
           return (
              <tr key={id}>
                 <td>{id}</td>
                 <td>{realm}</td>
                 <td onClick={()=>{this.handleShow(realm,false)}}>{username}</td>
                 <td onClick={()=>{this.handleShow(realm,true)}}>{password}</td>
              </tr>
           )
        })
    }
    render () {
        return (
            <>
            <br />
            <br />
            <MyModal 
            showModalPageOne={this.state.showModalPageOne} 
            handleClose={this.handleClose} 
            handleSave={this.save}
            modalProperties={this.modalProperties}>
          </MyModal>
        {
            (()=>{
                if (this.state.pageOneready==false){
                    return (<Table striped bordered hover />)
                  }
                  return (
                    <Table striped bordered hover>
                    <thead><tr>{this.renderTableHeader()}</tr></thead>
                    <tbody>
                        {this.renderTableData()}
                    </tbody>
                  </Table>
                  )
            })()
        }
        </>
        );
    }
}

export default Accounts;