import React, { Component } from "react";
import * as Settings from "../splunk/settings";
import { Form, Button, Row, Col } from 'react-bootstrap';


class Proxy extends Component {
    constructor({ props }) {
        super();
        this.state = {
          splunk_js_sdk: props.splunk_js_sdk,
          bodyProxy: <h1>Loading data... </h1>,
          stanza: undefined
        };
        this.open=false;
        this.enabled;
        this.protocol;
        this.port;
        this.address;
      }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    componentDidMount() {
      this.getProxy();
    }

    async off() {
      let prop = {
        enabled: false,
        address : this.state.stanza.properties()['address'],
        port : this.state.stanza.properties()['port'],
        protocol : this.state.stanza.properties()['protocol']
      }
      let res = await Settings.setConfigurationStanza(this.state.stanza,prop);
      this.setState({bodyProxy: <h1>Reloading data...</h1>});
      await this.sleep(400);
      await this.getProxy();
    }
    async on() {        
      await this.getProxy(this.open);
    }
    async change() {
      if(this.state.stanza.properties()['enabled']==0 && this.open==false){
        //jest wyłączone
        this.open=true;
        await this.on();
      }else if(this.state.stanza.properties()['enabled']==0 && this.open==true){
        this.open=false;
        await this.on();
      }else{
        //jest włączone       
        await this.off();
      }
    }
    async check(){
      this.open=false;
      let prop = { enabled: true}
      if(this.protocol!==undefined && this.protocol !== ""){
        prop["protocol"] = this.protocol;
      }else{
        prop["protocol"] = this.state.stanza.properties()['protocol']
      }
      if(this.port!==undefined && this.port !== ""){
        prop["port"] = this.port;
      }else{
        prop["port"] = this.state.stanza.properties()['port']
      }
      if(this.address!==undefined && this.address !== ""){
        prop["address"] = this.address;
      }else{
        prop["address"] = this.state.stanza.properties()['address']
      }
      let res = await Settings.setConfigurationStanza(this.state.stanza,prop);
      this.setState({bodyProxy: <h1>Reloading data...</h1>});
      await this.sleep(400);
      await this.getProxy();

    }
    async getProxy(newForm = false){
      let confFile = await Settings.getConfigurationFile(this.state.splunk_js_sdk,"proxy");
      let confStanza = await Settings.getConfigurationStanza(confFile,"settings");
      let xyz = confStanza.properties()['enabled'];
      let bof = {}
      if(xyz==0 && newForm==false){
        bof = {
          v:"success",
          value:"On"
        }
        this.setState({
          bodyProxy: 
          <Form>
            <Row className="mb-12">
                    <Button variant={bof['v']} onClick={ async () => { await this.change()}}>{bof['value']}</Button>
            </Row>
            <>
            <br />
            </>
          </Form>,
          stanza: confStanza
          });
      }else{
        if(newForm){
          bof = {
            v:"info",
            value: "Fill in the fields if needed",
            valueAp: "Turn on",
            vApp: "success"
          }
        }else{
          bof = {
            v:"danger",
            value:"Off",
            valueAp : "Apply",
            vApp: "primary"
          }
        }

      
      this.setState({
        bodyProxy: 
        <Form>
          <Row className="mb-12">
                  <Button variant={bof['v']} onClick={ async () => { await this.change()}}>{bof['value']}</Button>
          </Row>
          <>
          <br />
          </>
          <Row className="mb-12">
            <Col className="mb-6">
              <Row className="mb-12">
                <Col className="mb-4" />
                <Col className="mb-4">
                  Current settings
                </Col>
                <Col className="mb-4" />
              </Row>
            </Col>
            <Col className="mb-6" >
              New settings: if left blank, the data will be the same as in current settings. You do not need to complete all the data.
            </Col>
          </Row>
          <Row className="mb-12">
            <Col className="mb-8">
              <Form.Group as={Row} className="mb-3" controlId="formPlainTextEnabled">
                <Form.Label column sm="4">Enabled</Form.Label>
                <Col sm="8">
                  <Form.Control style={{color: "black"}}  readOnly defaultValue={confStanza.properties()['enabled']} />
                </Col>
              </Form.Group>
            </Col>
            <Col className="mb-4">
              <Form.Group as={Row} className="mb-12">

              </Form.Group>
            </Col>
          </Row>
          <Row className="mb-12">
            <Col className="mb-8">
              <Form.Group as={Row} className="mb-3" controlId="formPlainTextAddress">
                <Form.Label column sm="4">Address</Form.Label>
                <Col sm="8">
                  <Form.Control style={{color: "black"}}  readOnly defaultValue={confStanza.properties()['address']} />
                </Col>
              </Form.Group>
            </Col>
            <Col className="mb-4">
              <Form.Group as={Row} className="mb-12" controlId="formPlainTextAddressNew">
                <Col sm="8">
                  <Form.Control placeholder="Enter new address" onChange={e => this.address=e.target.value}  />
                </Col>
              </Form.Group>
            </Col>
          </Row>
          <Row className="mb-12">
            <Col className="mb-8">
              <Form.Group as={Row} className="mb-3" controlId="formPlainTextPort">
                <Form.Label column sm="4">Port</Form.Label>
                <Col sm="8">
                  <Form.Control style={{color: "black"}}  readOnly defaultValue={confStanza.properties()['port']} />
                </Col>
              </Form.Group>
            </Col>
            <Col className="mb-4">
              <Form.Group as={Row} className="mb-12" controlId="formPlainTextPortNew">
                <Col sm="8">
                  <Form.Control placeholder="Enter new port" onChange={e => this.port=e.target.value} />
                </Col>
              </Form.Group>
            </Col>
          </Row>
          <Row className="mb-12">
            <Col className="mb-8">
              <Form.Group as={Row} className="mb-3" controlId="formPlainTextProtocol">
                <Form.Label column sm="4">Protocol</Form.Label>
                <Col sm="8">
                  <Form.Control style={{color: "black"}}  readOnly defaultValue={confStanza.properties()['protocol']} />
                </Col>
             </Form.Group>
            </Col>
            <Col className="mb-4">
              <Form.Group as={Row} className="mb-12" controlId="formPlainTextProtocolNew">
                <Col sm="8">
                  <Form.Control placeholder="Enter new protocol (http/https)" onChange={e => this.protocol=e.target.value}/>
                </Col>
              </Form.Group>
            </Col>
          </Row>
          <Row className="mb-12">
            <Col className="mb-8">

            </Col>
            <Col className="mb-4">
              <Button variant={bof['vApp']}onClick={async () => {await this.check()}}>{bof['valueAp']}</Button>
            </Col>

          </Row>

      </Form>,
      stanza: confStanza
      });
      }
    }
    render () {
        return (<>
        <br />
        <br />
          {this.state.bodyProxy}
          </>
        );
    }
}

export default Proxy;