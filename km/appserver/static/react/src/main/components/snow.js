import React, { Component } from "react";
import {Form,Row,Col, Button} from 'react-bootstrap';
import * as Settings from "../splunk/settings";

class Snow extends Component {
    constructor({ props }) {
        super();
        this.state = {
          splunk_js_sdk: props.splunk_js_sdk,
          body: <h1> Loading data...</h1>,
          stanza: undefined
        };
        this.name;       
    }


    componentDidMount() {
      this.build();
    }
  
    componentWillUnmount() {
    }

    async save(){
      if(this.name==undefined){
        return;
      }
      this.setState({body: <h1>Reloading data...</h1>});
      let prop = {instance_name: this.name}
      let res = await Settings.setConfigurationStanza(this.state.stanza,prop);
      await this.build();
      this.name=undefined;
    }

    async build(){
      let confFile = await Settings.getConfigurationFile(this.state.splunk_js_sdk,"snow");
      let confStanza = await Settings.getConfigurationStanza(confFile,"settings");
      this.setState({
        body: 
        <Form>
          <Row className="mb-12">
            <Col className="mb-8">
              <Form.Group as={Row} className="mb-3" controlId="formInstanceName">
                <Form.Label column sm="2">Instance name</Form.Label>
                <Col sm="10">
                  <Form.Control style={{color: "black"}}  readOnly defaultValue={confStanza.properties()['instance_name']} />
                </Col>
              </Form.Group>
            </Col>
            <Col className="mb-4">
              <Form.Group as={Row} className="mb-12" controlId="formInstanceNameNew">
                <Col sm="8">
                  <Form.Control placeholder="Enter new instance name" onChange={e => this.name=e.target.value}  />
                </Col>
                <Col sm="4">
                  <Button  onClick={ async () => { await this.save()}}>Apply</Button>
                </Col>
              </Form.Group>
            </Col>
          </Row>
        </Form>,
        stanza: confStanza
      });
    }

    render () {
        return (
            <>
           <br />
           <br />
           {this.state.body}
            </>
        );
    }
}

export default Snow;