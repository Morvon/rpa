import React from "react";
import {Table} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import * as StoreSecret from "./splunk/store_secret.js";
import MyModal from "./elements/MyModal.js";
import MyNavbar from "./elements/MyNavbar.js";
import Accounts from "./components/accounts.js";

class App extends React.Component {
  constructor({ props }) {
    super();
    console.log(props)
    this.state = {
      text: props.text,
      searchManager: props.SearchManager,
      mvc: props.mvc,
      splunk_js_sdk: props.splunk_js_sdk,
      searchResult: [],
      page: 1,
      pageOneready: false,
      accounts: [],
      showModalPageOne: false
    };
    this.modalProperties={title:"",msg:"", type:""};
    this.newValue = "";
    this.handleClose = this.handleClose.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.changePage = this.changePage.bind(this);
    this.build();
  }

  handleClose = () =>  this.setState({ showModalPageOne: false })
  handleShow = (realm,pass) =>  {
    if(pass==true){
      this.modalProperties={
        title:"Change Password",
        msg:"Enter new password",
        type:"password"
      }
    }else{
      this.modalProperties={
        title:"Change Username",
        msg:"Enter new username",
        type:"text"
      }
    }
    this.setState({ showModalPageOne: true })

  }
  handleSave = (val) => {
    console.log(this.newValue);
    console.log(val);
    this.setState({ showModalPageOne: false })
  }
  async build(){
  const credentials = await StoreSecret.getCredentials(this.state.splunk_js_sdk);
  let acc = [];
  let ID=1;
  for(const credential of credentials){
    let data = credential.properties();
    acc.push({ 
      id: ID,
      realm: data['realm'],
      username: data['username'],
      password: data['encr_password']
     });
    ID++;
  }
  this.setState({accounts:acc, pageOneready: true});
  }

  changePage = (id) => {
      this.setState({page: id})
  }
  renderTableHeader() {
    let header = Object.keys(this.state.accounts[0])
    return header.map((key, index) => {
       return <th key={index}>{key.toUpperCase()}</th>
    })
 };
renderTableData() {
    return this.state.accounts.map((account, index) => {
       const { id, realm, username, password } = account
       return (
          <tr key={id}>
             <td>{id}</td>
             <td>{realm}</td>
             <td onClick={()=>{this.handleShow(realm,false)}}>{username}</td>
             <td onClick={()=>{this.handleShow(realm,true)}}>{password}</td>
          </tr>
       )
    })
}

  render() {
    return (
        <>
        <MyNavbar
        changePage={this.changePage}
        >
        </MyNavbar>

        { (()=> {
          if(this.state.page===1){

          }else{
            return (<Accounts props={{splunk_js_sdk:this.state.splunk_js_sdk}}/>)
          }
        })()
        }
      </>
    );
  }
}

export default App;