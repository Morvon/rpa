import { Button, Modal, Form} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from "react";



const MyModal = ({ showModalPageOne, handleClose, handleSave, modalProperties }) => {
    const [value, setValue] = useState("");
    if(modalProperties==undefined){
      modalProperties = {title:"",msg:"", type:""}
    }
    return (
    <Modal animation={false} style={{backgroundColor:"transparent",     marginTop: '0px !important',
    marginLeft: 'auto',
    marginRight: 'auto'}} show={showModalPageOne} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>{modalProperties.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{modalProperties.msg} 
        <Form.Control type={modalProperties.type} placeholder="Enter new" onChange={e => setValue(e.target.value)}/>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={() => {handleSave(value)}}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    )
}


export default MyModal;