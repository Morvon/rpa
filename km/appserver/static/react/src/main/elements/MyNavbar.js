import {Navbar, Nav, Container} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import React from "react";

const MyNavbar = ({changePage}) => {
    return (
        <Navbar bg="light" expand="lg" variant="light">
          <Container>
          <Navbar.Brand href="#home">Navbar</Navbar.Brand>
            <Nav className="ml-auto">
                <Nav.Link onClick={() => {changePage(1)}}>
                    Accounts
                </Nav.Link>
                <Nav.Link onClick={() => {changePage(3)}}>
                    Service Now
                </Nav.Link>
                <Nav.Link onClick={() => {changePage(2)}}>
                    Proxy
                </Nav.Link>
            </Nav>
        </Container>
      </Navbar>
    );
}


export default MyNavbar;