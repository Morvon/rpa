async function reload_splunk_app(
  splunk_js_sdk_service,
  app_name,
) {
  var splunk_js_sdk_apps = splunk_js_sdk_service.apps();
  await splunk_js_sdk_apps.fetch();
  var current_app = splunk_js_sdk_apps.item(app_name);
  await current_app.reload();
};

async function get_credentials_app(splunk_js_sdk_service){
  var storagePasswords = splunk_js_sdk_service.storagePasswords();
 await storagePasswords.fetch();
 return storagePasswords.list();
}

function create_splunk_js_sdk_service(
  splunk_js_sdk,
  application_name_space,
) {
  var http = new splunk_js_sdk.SplunkWebHttp();
  var splunk_js_sdk_service = new splunk_js_sdk.Service(
      http,
      application_name_space,
  );
  return splunk_js_sdk_service;
};



export {
  reload_splunk_app,
  create_splunk_js_sdk_service,
  get_credentials_app
}
