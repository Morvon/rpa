"use strict";
import * as Config from './configuration.js'
var app_name = "km";

var application_name_space = {
    owner: "nobody",
    app: app_name,
    sharing: "app",
};

export async function getConfigurationFile(splunk_js_sdk,nameFile){
    try{
        const service = Config.create_splunk_js_sdk_service(
            splunk_js_sdk,
            application_name_space,
        );
        let con = await service.configurations(application_name_space);
        await con.fetch();
        let confFile = con.instantiateEntity({name:nameFile});
        await confFile.fetch();
        return confFile;   
    }catch(error){
        console.log('Error:', error);
        alert('Error:' + error);
    }
}

export async function getConfigurationStanza(configurationFile,nameStanza){
    try{
        let confStanza = configurationFile.instantiateEntity({name:nameStanza});
        await confStanza.fetch();
        return confStanza;   
    }catch(error){
        console.log('Error:', error);
        alert('Error:' + error);
    }
}

export async function setConfigurationStanza(configurationStanza, properties){
    try{

        let res = configurationStanza.post("",properties);
        return res;   
    }catch(error){
        console.log('Error:', error);
        alert('Error:' + error);
    }
}