"use strict";
import * as Config from './configuration.js'
var app_name = "km";

var application_name_space = {
    owner: "nobody",
    app: app_name,
    sharing: "app",
};

export async function getCredentials(splunk_js_sdk){
    try{
        const service = Config.create_splunk_js_sdk_service(
            splunk_js_sdk,
            application_name_space,
    );
    var credentials = await Config.get_credentials_app(service)
    //console.log(credentials);
    return credentials;
    }catch(error){
        console.log('Error:', error);
        alert('Error:' + error);
    }
}

export async function setPasswordIntoCredentials(splunk_js_sdk,realm,newPass){
    try{
        const service = Config.create_splunk_js_sdk_service(
            splunk_js_sdk,
            application_name_space,
        );

        var credentials = await Config.get_credentials_app(service)
        for(const credential of credentials){
            let data = credential.properties();
            if(data['realm']==realm){
                credential.post("",{password:newPass})
            }         
        }
        await Config.reload_splunk_app(service, app_name);
    }catch(error){
        console.log('Error:', error);
        alert('Error:' + error);
    }
}
export async function setUsernameIntoCredentials(splunk_js_sdk,realm,newUsername){
    try{
        const service = Config.create_splunk_js_sdk_service(
            splunk_js_sdk,
            application_name_space,
        );

        var credentials = await Config.get_credentials_app(service)
        let data;
        for(const credential of credentials){
            data = credential.properties();
            if(data['realm']==realm){
                credential.del();
                break;
            }   

        }
        var storagePasswords = service.storagePasswords();
        storagePasswords.create({
            name: newUsername, 
            realm: data['realm'], 
            password: data['clear_password']
        }, 
            function(err, storagePassword) {
              if (err) 
                  {console.warn(err);}
              else {
               console.log(storagePassword.properties());
               }
            }
        );

        await Config.reload_splunk_app(service, app_name);
    }catch(error){
        console.log('Error:', error);
        alert('Error:' + error);
    }
}