import React from "react";
import 'bootstrap/dist/css/bootstrap.css'
import {Form, Col, Row, Button, ButtonGroup, ToggleButton, ToggleButtonGroup} from 'react-bootstrap';
import * as Setup from "./store_secret.js";

class App extends React.Component {
  constructor({ props }) {
    super();
    this.state = {
      text: props.text,
      searchManager: props.SearchManager,
      mvc: props.mvc,
      splunk_js_sdk: props.splunk_js_sdk,
      searchResult: [],
    };
    this.snowUsername;
    this.snowPassword;
    this.proxyUsername;
    this.proxyPassword;
    this.instanceName;
    this.protocol;
    this.address;
    this.port;
    this.radioValue=1;
    this.proxyValue=1;
  }

  async setup(){
    let props = {
    }
    if(this.snowUsername==undefined || this.snowUsername=="" || this.snowPassword=="" || this.snowPassword==undefined || this.instanceName=="" || this.instanceName==undefined){
        alert("Enter Service Now Category!");
        return;
    }else{
        props["usernameSnow"] = this.snowUsername;
        props["passwordSnow"] = this.snowPassword;
        props["instanceName"] = this.instanceName;
    }
    if(this.proxyValue==1){
        props["proxy"] = true;
        if(this.protocol==undefined || this.protocol=="" || this.port==undefined || this.port=="" || this.address==undefined || this.address==""){
            alert("Enter Proxy Category or turn off!");
            return;
        }else{
            props["protocol"] = this.protocol;
            props["port"] = this.port;
            props["address"] = this.address;
            if(this.radioValue==1 && (this.proxyUsername==undefined ||this.proxyUsername=="" || this.proxyPassword == undefined || this.proxyPassword == "")){
                alert("Enter Proxy Username/Password");
                return;
            }else if(this.radioValue==1){
                props["loginProxy"] = true;
                props["usernameProxy"] = this.proxyUsername;
                props["passwordProxy"] = this.proxyPassword;
            }else{
                props["loginProxy"] = false;
                props["usernameProxy"] = "";
                props["passwordProxy"] = "";
            }
        }

    }else{
        props["proxy"] = false;
        props["protocol"] = "";
        props["port"] = "";
        props["address"] = "";
        props["loginProxy"] = false;
        props["usernameProxy"] = "";
        props["passwordProxy"] = "";
    }
    console.log(props);
    await Setup.perform(this.state.splunk_js_sdk, props)
  }

  render() {

    return (
       <>
       <Form>
           <Row sm="12">
               <Col sm="12">
                   <h1 className="text-center"> Service Now settings</h1>
               </Col>
           </Row>
           <Row>
               <Col>
               <Form.Group as={Row} className="mb-3" controlId="formSnowUsername">
                    <Form.Label column sm="2">Username</Form.Label>
                    <Col sm="10">
                        <Form.Control onChange={ e => this.snowUsername=e.target.value}/>
                    </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3" controlId="formSnowPassword">
                    <Form.Label column sm="2">Password</Form.Label>
                    <Col sm="10">
                        <Form.Control type="password" onChange={ e => this.snowPassword=e.target.value}/>
                    </Col>
                </Form.Group>
               </Col>
               <Col>
               <Form.Group as={Row} className="mb-3" controlId="formInstanceNameNew">
                    <Form.Label column sm="2">Instance name</Form.Label>
                    <Col sm="10">
                        <Form.Control onChange={ e => this.instanceName=e.target.value}/>
                    </Col>
                </Form.Group>
               </Col>
           </Row>
           <Row sm="12">
               <Col sm="12">
                   <h1 className="text-center"> Proxy settings</h1>
               </Col>
           </Row>
           <Row>
               <Col>
               <Form.Group as={Row} className="mb-3" controlId="formProxyLogin">
                    <Form.Label column sm="2">Proxy</Form.Label>
                    <Col sm="10">
                    <ButtonGroup>
                        <ToggleButtonGroup type="radio" name="options2" defaultValue={1}>
                            <ToggleButton id="tbg-radio-11" value={1} variant="success" onClick={e => this.proxyValue=e.target.value}>
                                On
                            </ToggleButton>
                            <ToggleButton id="tbg-radio-22" value={2} variant="danger" onClick={e => this.proxyValue=e.target.value}>
                                Off
                            </ToggleButton>
                        </ToggleButtonGroup>
                    </ButtonGroup>
                    </Col>
                </Form.Group>
               <Form.Group as={Row} className="mb-3" controlId="formProxyLogin">
                    <Form.Label column sm="2">Login into proxy?</Form.Label>
                    <Col sm="10">
                    <ButtonGroup>
                        <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
                            <ToggleButton id="tbg-radio-1" value={1} variant="success" onClick={e => this.radioValue=e.target.value}>
                                On
                            </ToggleButton>
                            <ToggleButton id="tbg-radio-2" value={2} variant="danger" onClick={e => this.radioValue=e.target.value}>
                                Off
                            </ToggleButton>
                        </ToggleButtonGroup>
                    </ButtonGroup>
                    </Col>
                </Form.Group>
               <Form.Group as={Row} className="mb-3" controlId="formproxyUsername">
                    <Form.Label column sm="2">Username</Form.Label>
                    <Col sm="10">
                        <Form.Control onChange={ e => this.proxyUsername=e.target.value}/>
                    </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3" controlId="formProxyPassword">
                    <Form.Label column sm="2">Password</Form.Label>
                    <Col sm="10">
                        <Form.Control type="password" onChange={ e => this.proxyPassword=e.target.value}/>
                    </Col>
                </Form.Group>
               </Col>
               <Col>
               <Form.Group as={Row} className="mb-3" controlId="formaddress">
                    <Form.Label column sm="2">Address</Form.Label>
                    <Col sm="10">
                        <Form.Control onChange={ e => this.address=e.target.value}/>
                    </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3" controlId="formport">
                    <Form.Label column sm="2">Port</Form.Label>
                    <Col sm="10">
                        <Form.Control onChange={ e => this.port=e.target.value}/>
                    </Col>
                </Form.Group>
                <Form.Group as={Row} className="mb-3" controlId="formProtocol">
                    <Form.Label column sm="2">Protocol</Form.Label>
                    <Col sm="10">
                        <Form.Control onChange={ e => this.protocol=e.target.value} placeholder="Enter http/https"/>
                    </Col>
                </Form.Group>
               </Col>
           </Row>
           <Row>
               <Button onClick={async () => {
                   await this.setup();
               }}>
                   Send
               </Button>
           </Row>
       </Form>
      </>
    );
  }
}

export default App;