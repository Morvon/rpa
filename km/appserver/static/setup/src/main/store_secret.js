"use strict";


import * as Config from './setup_configuration.js'

export async function perform(splunk_js_sdk, setup_options) {
    var app_name = "km";

    var application_name_space = {
        owner: "nobody",
        app: app_name,
        sharing: "app",
    };

    try {
        const service = Config.create_splunk_js_sdk_service(
                splunk_js_sdk,
                application_name_space,
        );

        let props = setup_options;

        var storagePasswords = service.storagePasswords();
 
        storagePasswords.create({
            name: props['usernameSnow'], 
            realm: "ServiceNow", 
            password: props['passwordSnow']
        }, 
            function(err, storagePassword) {
              if (err) 
                  {console.warn(err);}
              else {
               console.log(storagePassword.properties());
               }
            }
        );
        if(props['loginProxy']==true){
            storagePasswords.create({
                name: props['usernameProxy'], 
                realm: "Proxy", 
                password: props['passwordProxy']
            }, 
                function(err, storagePassword) {
                  if (err) 
                      {console.warn(err);}
                  else {
                   console.log(storagePassword.properties());
                   }
                }
            );
        }
 
        await Config.complete_setup(service,setup_options);

        await Config.reload_splunk_app(service, app_name);

        Config.redirect_to_splunk_app_homepage(app_name);
        } catch (error) {

        console.log('Error:', error);
        alert('Error:' + error);
    }
}
